import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject, of } from "rxjs";
import { FilmsList } from "./films-list";
import { FilmsConfig } from "./filmsconfig";

@Injectable({
  providedIn: "root"
})
export class FilmsService {
  private message = new Subject<any>();
  message$ = this.message.asObservable();

  setParams(val) {
    this.message.next(val);
  }

  constructor(private http: HttpClient) {}

  getFilms(term: string): Observable<FilmsList> {
    if (!term.trim()) {
      return of();
    }
    //I just want to mention that yes I am aware that I have not obscured my key. In a real world scenerio, I would never let you see my key. Though I suppose this is the real world. Please don't steal my films key. Next time you look, poof it will be gone.
    let requestPath = `https://api.themoviedb.org/3/search/movie?query=${encodeURI(
      term
    )}&api_key=633723395e17100a2abf106dc9bea686&page=1`;

    return this.http.get<FilmsList>(requestPath);
  }

  getFilmConfig() {
    let requestPath =
      "https://api.themoviedb.org/3/configuration?api_key=633723395e17100a2abf106dc9bea686";
    return this.http.get<any>(requestPath);
  }
}
