import { Component, OnInit } from "@angular/core";
import { FilmsService } from "../films.service";
import { Observable } from "rxjs";
import { FormControl } from "@angular/forms";
import { FilmsConfig } from "../filmsconfig";
import { FilmsList } from "../films-list";
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
  map
} from "rxjs/operators";

@Component({
  selector: "app-films",
  templateUrl: "./films.component.html",
  styleUrls: ["./films.component.scss"]
})
export class FilmsComponent implements OnInit {
  filmsList$: Observable<FilmsList[]>;
  public searchField: FormControl;
  public loading: boolean = false;
  public config: FilmsConfig;
  public viewport: number = window.innerWidth;
  public noResults: boolean = false;
  message: any;

  constructor(private filmsService: FilmsService) {
    filmsService.message$.subscribe(message => {
      this.message = message;
    });
  }

  setConfig() {
    let imageSize;

    if (this.viewport >= 425) {
      imageSize = 1;
    } else {
      imageSize = 4;
    }

    this.filmsService
      .getFilmConfig()
      .subscribe(
        data =>
          (this.config = new FilmsConfig(
            data.images.base_url,
            data.images.poster_sizes[imageSize]
          ))
      );
  }

  formatResults(term: string) {
    return this.filmsService.getFilms(term).pipe(
      map(res => {
        const { results } = res as any;

        if (results.length === 0) {
          this.noResults = true;
          return;
        }

        this.noResults = false;

        return results.map(item => {
          const {
              id,
              original_title,
              overview,
              release_date,
              poster_path,
              vote_average,
              vote_count
            } = item,
            { base_url, poster_sizes } = this.config;
          let fullPosterPath, imageSize;
          //set image size for desktop or mobile
          //@TODO do something about image size on resize
          if (this.viewport >= 425) {
            imageSize = "154";
          } else {
            imageSize = "500";
          }
          //if no poster image set a placeholder
          if (poster_path === null) {
            fullPosterPath = `https://via.placeholder.com/${imageSize}?text=No+Image`;
          } else {
            fullPosterPath = base_url + poster_sizes + poster_path;
          }

          return new FilmsList(
            id,
            original_title,
            overview,
            release_date,
            fullPosterPath,
            vote_average,
            vote_count
          );
        });
      })
    );
  }

  openModal(message: string): void {
    this.filmsService.setParams(message);
  }

  ngOnInit() {
    this.setConfig();
    this.searchField = new FormControl();
    this.filmsList$ = this.searchField.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      tap(_ => (this.loading = true)),
      switchMap(term => this.formatResults(term)),
      tap(_ => (this.loading = false))
    );
  }
}
