import { Component, OnInit } from "@angular/core";
import { FilmsService } from "../films.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit {
  openModal: boolean = false;
  message: string;

  constructor(private filmsService: FilmsService) {
    filmsService.message$.subscribe(message => {
      this.message = message;
      this.openModal = true;
    });
  }

  ngOnInit() {}
}
