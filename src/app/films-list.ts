export class FilmsList {
  constructor(
    public id: number,
    public original_title: string,
    public overview: string,
    public release_date: string,
    public poster_path: string,
    public vote_average: string,
    public vote_count: string
  ) {}
}
